# Christian Chown - Black Swan Test

Hello. I've reworked https://bitbucket.org/blackswanclient/blackswan-ios-test as a React Native app

## Install / run

1. Clone this repo
2. `npm install`
3. `npm install -g react-native-cli` if you haven't already
4. `react-native run-android` or `react-native run-ios`

You'll probably need emulators/Xcode/adb for those to work.

Alternatively, there's a signed Android release you can grab at https://gitlab.com/christianchown/blackswan/blob/master/android/app-release.apk

## Lint / test

- ESLint: `npm run lint`
- Jest/Enzyme: `npm run test`

## Packages

- I added in a couple of native components - https://github.com/crazycodeboy/react-native-splash-screen and https://github.com/yamill/react-native-orientation for a bit of polish
- I could have used https://github.com/austinkelleher/giphy-api, but it was a simple enough `fetch` so I didn't bother
- I could have done the whole thing in state rather than using Redux, but props > state and async thunks ftw
- For even better test coverage, I would use redux-mock-store and passing mock states to my connected components, but hope you get the idea
- I like Sindre Sorhus' https://github.com/sindresorhus/react-extras for declarative goodness, but it didn't play nice with an Android release build, so in the end I just cannibalised the If component

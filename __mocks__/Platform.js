const mockPlatform = {
  OS: 'android',
  Version: 23,
  setOS: (os) => {
    mockPlatform.OS = os;
    mockPlatform.Version = os === 'android' ? 23 : 10;
  },
};

module.exports = mockPlatform;

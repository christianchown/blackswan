const mockLinking = {
  addEventListener: jest.fn(),
  removeEventListener: jest.fn(),
  openURL: jest.fn().mockReturnValue(Promise.resolve(true)),
  canOpenURL: jest.fn(),
};

export default mockLinking;

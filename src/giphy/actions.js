import config from '../config';

export const GIPHY_SEARCH = 'GIPHY_SEARCH';
export const GIPHY_SEARCH_RESULT = 'GIPHY_SEARCH_RESULT';
export const GIPHY_ERROR = 'GIPHY_ERROR';

export const giphySearch = term => ({
  type: GIPHY_SEARCH,
  term,
});

export const giphySearchResult = (term, gifs) => ({
  type: GIPHY_SEARCH_RESULT,
  term,
  gifs,
});

export const giphyError = error => ({
  type: GIPHY_ERROR,
  error,
});

const fetchRetrieve = term => {
  const url = `https://api.giphy.com/v1/gifs/search?api_key=${
    config.giphyAPIKey
  }&limit=25&offset=0&rating=G&lang=en&q=${encodeURIComponent(term)}`;
  return fetch(url, {
    mode: 'cors',
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });
};

export const retrieve = term => async dispatch => {
  dispatch(giphySearch(term));
  try {
    const JSONresponse = await fetchRetrieve(term);
    const response = await JSONresponse.json();
    const {meta, data} = response;
    if (meta && meta.status === 200) {
      dispatch(giphySearchResult(term, data));
      return Promise.resolve(data);
    }
    dispatch(giphyError(`Status ${(meta || {}).status}: ${(meta || {}).msg}`));
    return Promise.resolve(false);
  } catch (error) {
    dispatch(giphyError(error.message || 'unknown search error'));
    return Promise.resolve(false);
  }
};

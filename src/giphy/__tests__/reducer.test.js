import * as actions from '../actions';
import exampleGif from '../__mocks__/exampleGif';
import reducer, {initialState} from '../reducer';

describe('reducer', () => {
  test('GIPHY_SEARCH', () => {
    expect(
      reducer(initialState, actions.giphySearch('term')),
    ).toMatchSnapshot();
  });
  test('GIPHY_SEARCH_RESULT', () => {
    expect(
      reducer(initialState, actions.giphySearchResult('term', [exampleGif])),
    ).toMatchSnapshot();
  });
  test('GIPHY_ERROR', () => {
    expect(
      reducer(initialState, actions.giphyError('message')),
    ).toMatchSnapshot();
  });
});

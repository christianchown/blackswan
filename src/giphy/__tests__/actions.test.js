import * as actions from '../actions';
import exampleGif from '../__mocks__/exampleGif';

describe('actions', () => {
  test('GIPHY_SEARCH', () => {
    expect(actions.giphySearch('search', 'term')).toMatchSnapshot();
  });
  test('GIPHY_SEARCH_RESULT', () => {
    expect(
      actions.giphySearchResult('search', 'term', [exampleGif]),
    ).toMatchSnapshot();
  });
  test('GIPHY_ERROR', () => {
    expect(actions.giphyError('message')).toMatchSnapshot();
  });
});

describe('retrieve thunk', () => {
  let dispatch;
  beforeEach(() => {
    dispatch = jest.fn();
  });
  test('ok', async () => {
    const mockResponse = {
      data: [exampleGif],
      pagination: {
        total_count: 1,
        count: 1,
        offset: 0,
      },
      meta: {
        status: 200,
        msg: 'OK',
        response_id: 'response_id',
      },
    };
    fetch.mockResponse(JSON.stringify(mockResponse));
    const result = await actions.retrieve('term')(dispatch);
    expect(dispatch.mock.calls).toMatchSnapshot();
    expect(result).toMatchSnapshot();
  });
  test('error', async () => {
    const mockResponse = {
      data: [],
      pagination: {
        total_count: 0,
        count: 0,
        offset: 0,
      },
      meta: {
        status: 500,
        msg: 'Internal server error',
        response_id: 'error_id',
      },
    };
    fetch.mockResponse(JSON.stringify(mockResponse));
    const result = await actions.retrieve('term')(dispatch);
    expect(dispatch.mock.calls).toMatchSnapshot();
    expect(result).toMatchSnapshot();
  });
});

import {GIPHY_SEARCH, GIPHY_SEARCH_RESULT, GIPHY_ERROR} from './actions';

import {defaultState} from './propTypes';

export const initialState = defaultState;

export default (state = initialState, action) => {
  switch (action.type) {
    case GIPHY_SEARCH:
      if (
        state.searches[action.term] &&
        state.searches[action.term].retrieved
      ) {
        return state;
      }
      return {
        ...state,
        searches: {
          ...state.searches,
          [action.term]: {
            retrieved: false,
            gifs: [],
          },
        },
      };

    case GIPHY_SEARCH_RESULT:
      return {
        ...state,
        searches: {
          ...state.searches,
          [action.term]: {
            retrieved: true,
            gifs: action.gifs,
          },
        },
      };

    case GIPHY_ERROR:
      return {
        ...state,
        error: action.error,
      };

    default:
      return state;
  }
};

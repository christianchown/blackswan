import PropTypes from 'prop-types';

const If = ({condition, children, render}) =>
  condition ? (render ? render() : children) : null;

If.propTypes = {
  condition: PropTypes.bool.isRequired,
  children: PropTypes.node,
  render: PropTypes.func,
};

export default If;

import React from 'react';
import PropTypes from 'prop-types';
import {Platform, StyleSheet, View, TextInput} from 'react-native';
import {connect} from 'react-redux';

import config from '../config';
import {focusText, updateText} from '../filter/actions';
import {retrieve} from '../giphy/actions';

const styles = StyleSheet.create({
  container: {
    borderColor: config.colours.lightText,
    borderWidth: 1,
  },
  androidTextInput: {
    textAlign: 'center',
    fontFamily: config.fonts.lato,
    fontSize: 28,
    color: config.colours.lightText,
    width: 300,
  },
  iosTextInput: {
    textAlign: 'center',
    fontFamily: config.fonts.lato,
    fontSize: 23,
    color: config.colours.lightText,
    width: 240,
  },
});

export class DumbSearchBox extends React.Component {
  blur = () => {
    const {dispatch} = this.props;
    dispatch(focusText(false));
  };

  focus = () => {
    const {dispatch} = this.props;
    dispatch(focusText(true));
  };

  change = text => {
    const {dispatch} = this.props;
    dispatch(updateText(text));
  };

  submit = () => {
    const {dispatch, text} = this.props;
    if (text) {
      dispatch(retrieve(text));
    }
  };

  render() {
    const {text} = this.props;
    return (
      <View style={styles.container}>
        <TextInput
          placeholder="Search"
          placeholderTextColor={config.colours.midText}
          style={styles[`${Platform.OS}TextInput`]}
          onChangeText={this.change}
          onBlur={this.blur}
          onFocus={this.focus}
          maxLength={20}
          value={text}
          returnKeyType="search"
          underlineColorAndroid="transparent"
          onEndEditing={this.submit}
          autoCorrect={false}
        />
      </View>
    );
  }
}

DumbSearchBox.propTypes = {
  dispatch: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  text: state.filter.text,
});

export default connect(mapStateToProps)(DumbSearchBox);

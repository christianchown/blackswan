import React from 'react';
import {shallow} from 'enzyme';

import exampleGif from '../../giphy/__mocks__/exampleGif';
import {DumbSearchResults} from '../SearchResults';

const get = props => <DumbSearchResults {...props} />;

test('SearchResults.render empty', () => {
  const got = shallow(
    get({
      error: '',
      text: '',
      retrieved: false,
      retrieving: false,
      gifs: [],
    }),
  );
  expect(got).toMatchSnapshot();
});

test('SearchResults.render errored', () => {
  const got = shallow(
    get({
      error: 'error',
      text: 'text',
      retrieved: false,
      retrieving: false,
      gifs: [],
    }),
  );
  expect(got).toMatchSnapshot();
});

test('SearchResults.render retrieving', () => {
  const got = shallow(
    get({
      error: '',
      text: 'text',
      retrieved: false,
      retrieving: true,
      gifs: [],
    }),
  );
  expect(got).toMatchSnapshot();
});

test('SearchResults.render retrieved empty', () => {
  const got = shallow(
    get({
      error: '',
      text: 'text',
      retrieved: true,
      retrieving: false,
      gifs: [],
    }),
  );
  expect(got).toMatchSnapshot();
});

test('SearchResults.render retrieved not empty', () => {
  const got = shallow(
    get({
      error: '',
      text: 'text',
      retrieved: true,
      retrieving: false,
      gifs: [exampleGif, exampleGif],
    }),
  );
  expect(got).toMatchSnapshot();
});

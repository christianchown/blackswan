import React from 'react';
import {Linking} from 'react-native';
import {shallow} from 'enzyme';
import exampleGif from '../../giphy/__mocks__/exampleGif';

import GiphyThumbnail from '../GiphyThumbnail';

test('GiphyThumbnail.render', () => {
  expect(
    shallow(<GiphyThumbnail style={{flex: 1}} gif={exampleGif} />),
  ).toMatchSnapshot();
});

test.only('GiphyThumbnail.click', () => {
  const got = shallow(<GiphyThumbnail style={{flex: 1}} gif={exampleGif} />);
  got.find('[onPress]').forEach(element => {
    element.simulate('press');
  });
  expect(Linking.openURL.mock.calls).toMatchSnapshot();
});

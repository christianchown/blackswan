import React from 'react';
import {shallow} from 'enzyme';

import Loading from '../Loading';

test('Loading.render', () => {
  expect(shallow(<Loading />)).toMatchSnapshot();
});

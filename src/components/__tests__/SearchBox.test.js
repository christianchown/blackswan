import React from 'react';
import {shallow} from 'enzyme';

import {DumbSearchBox} from '../SearchBox';

const get = props => <DumbSearchBox {...props} />;

let dispatch;

beforeEach(() => {
  dispatch = jest.fn();
});

test('SearchBox.render', () => {
  const got = shallow(get({dispatch, text: 'text'}));
  expect(got).toMatchSnapshot();
});

test('SearchBox.api', () => {
  const got = shallow(get({dispatch, text: 'text'}));
  const instance = got.instance();
  instance.focus();
  instance.blur();
  instance.change('changed');
  instance.submit();
  expect(dispatch.mock.calls).toMatchSnapshot();
});

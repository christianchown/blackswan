import React from 'react';
import {shallow} from 'enzyme';

import {DumbGiphyViewer} from '../GiphyViewer';

test('GiphyViewer.render', () => {
  expect(
    shallow(<DumbGiphyViewer orientation="PORTRAIT" showResults />),
  ).toMatchSnapshot();
  expect(
    shallow(<DumbGiphyViewer orientation="LANDSCAPE" showResults />),
  ).toMatchSnapshot();
  expect(
    shallow(<DumbGiphyViewer orientation="unknown" showResults />),
  ).toMatchSnapshot();
  expect(
    shallow(<DumbGiphyViewer orientation="PORTRAIT" showResults={false} />),
  ).toMatchSnapshot();
  expect(
    shallow(<DumbGiphyViewer orientation="LANDSCAPE" showResults={false} />),
  ).toMatchSnapshot();
  expect(
    shallow(<DumbGiphyViewer orientation="unknown" showResults={false} />),
  ).toMatchSnapshot();
});

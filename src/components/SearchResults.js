import React from 'react';
import PropTypes from 'prop-types';
import {ScrollView, View, StyleSheet, Text} from 'react-native';
import {connect} from 'react-redux';
import If from './If';

import config from '../config';
import {giphyGIFShape} from '../giphy/propTypes';
import Loading from './Loading';
import GiphyThumbnail from './GiphyThumbnail';

const styles = StyleSheet.create({
  container: {},
  error: {
    margin: 20,
    color: config.colours.darkText,
    textAlign: 'center',
    fontFamily: config.fonts.lato,
    fontSize: 24,
  },
  loading: {
    margin: 20,
  },
  gifs: {
    flex: 1,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,
  },
  thumbnail: {
    borderColor: config.colours.lightText,
    borderWidth: 1,
    marginTop: 20,
  },
});

export const DumbSearchResults = ({
  retrieving,
  retrieved,
  gifs,
  error,
  text,
}) => (
  <ScrollView style={styles.container}>
    <If condition={!!error}>
      <Text style={styles.error}>An error has occurred - {error}</Text>
    </If>
    <If condition={retrieving}>
      <View style={styles.loading}>
        <Loading />
      </View>
    </If>
    <If condition={retrieved && gifs.length === 0}>
      <Text style={styles.error}>No gifs retrieved for {text}.</Text>
    </If>
    <If condition={gifs.length > 0}>
      <View style={styles.gifs}>
        {gifs.map(gif => (
          <GiphyThumbnail
            style={styles.thumbnail}
            gif={gif}
            key={`gif${gif.id}`}
          />
        ))}
      </View>
    </If>
  </ScrollView>
);

DumbSearchResults.propTypes = {
  error: PropTypes.string.isRequired,
  retrieving: PropTypes.bool.isRequired,
  retrieved: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  gifs: PropTypes.arrayOf(PropTypes.shape(giphyGIFShape)).isRequired,
};

const mapStateToProps = state => {
  const {filter, giphy} = state;
  const {text} = filter;
  const {error, searches} = giphy;
  return {
    error,
    text,
    retrieved: searches[text] ? searches[text].retrieved : false,
    retrieving: searches[text] ? !searches[text].retrieved : false,
    gifs: searches[text] ? searches[text].gifs || [] : [],
  };
};

export default connect(mapStateToProps)(DumbSearchResults);

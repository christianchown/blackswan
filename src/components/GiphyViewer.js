import React from 'react';
import PropTypes from 'prop-types';
import {
  Linking,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import If from './If';

import {Orientation} from '../orientation/propTypes';
import config from '../config';
import SearchBox from './SearchBox';
import SearchResults from './SearchResults';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    backgroundColor: config.colours.lightBG,
  },
  orientation_unknown: {},
  orientation_PORTRAIT: {
    flexDirection: 'column',
  },
  orientation_LANDSCAPE: {
    flexDirection: 'row',
  },
  header: {
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: config.colours.darkBG,
    flex: 1,
  },
  heading: {
    color: config.colours.lightText,
    textAlign: 'center',
    fontFamily: config.fonts.lato,
    fontSize: 24,
  },
  logo: {
    width: undefined,
    height: undefined,
    alignSelf: 'stretch',
    flex: 1,
  },
  button: {
    width: 64,
    height: 64,
  },
  results: {
    flex: 1,
  },
});

const gotoCode = () => {
  Linking.openURL('https://gitlab.com/christianchown/blackswan');
};

export const DumbGiphyViewer = ({showResults, orientation}) => (
  <View style={[styles.container, styles[`orientation_${orientation}`]]}>
    <View style={styles.header}>
      <Text style={styles.heading}>Black Swan Giphy Viewer</Text>
      <TouchableOpacity style={styles.button} onPress={gotoCode}>
        <Image
          resizeMode="contain"
          style={styles.logo}
          source={config.images.logo}
        />
      </TouchableOpacity>
      <SearchBox />
    </View>
    <If condition={showResults}>
      <View style={styles.results}>
        <SearchResults />
      </View>
    </If>
  </View>
);

DumbGiphyViewer.propTypes = {
  orientation: Orientation.isRequired,
  showResults: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  orientation: state.orientation,
  showResults:
    !state.filter.focused && Object.keys(state.giphy.searches).length > 0,
});

export default connect(mapStateToProps)(DumbGiphyViewer);

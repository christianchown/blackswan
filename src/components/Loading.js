import React from 'react';
import {ViewPropTypes, View, Easing, StyleSheet, Animated} from 'react-native';

import config from '../config';

const styles = StyleSheet.create({
  centered: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
});

class Loading extends React.Component {
  constructor(props) {
    super(props);
    this.state = {spin: new Animated.Value(0)};
  }

  componentWillMount() {
    this.spin();
  }

  spin = () => {
    this.state.spin.setValue(0);
    Animated.timing(this.state.spin, {
      toValue: 10,
      duration: 7000,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start(finished => {
      if (finished) {
        this.spin();
      }
    });
  };

  render() {
    const {style} = this.props;
    const {spin} = this.state;
    const rotate = spin.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg'],
    });
    const imageStyle = {
      transform: [{rotate}],
    };

    return (
      <View style={style}>
        <Animated.Image style={imageStyle} source={config.images.loading} />
      </View>
    );
  }
}

Loading.propTypes = {
  style: ViewPropTypes.style,
};

Loading.defaultProps = {
  style: styles.centered,
};

export default Loading;

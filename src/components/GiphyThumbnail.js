import React from 'react';
import PropTypes from 'prop-types';
import {
  TouchableWithoutFeedback,
  Linking,
  Image,
  Dimensions,
  StyleSheet,
  ViewPropTypes,
  View,
  Text,
} from 'react-native';
import If from './If';

import {giphyGIFShape} from '../giphy/propTypes';
import config from '../config';

const styles = StyleSheet.create({
  text: {
    color: config.colours.darkText,
    textAlign: 'center',
    fontFamily: config.fonts.lato,
    fontSize: 16,
  },
  imageText: {
    color: config.colours.darkText,
    textAlign: 'center',
    fontFamily: config.fonts.lato,
    fontSize: 16,
    marginTop: 10,
  },
  noDisplay: {
    paddingTop: 20,
    paddingBottom: 20,
  },
  imageWrap: {
    padding: 20,
    height: 260,
    alignItems: 'center',
  },
  thumbnail: {
    width: undefined,
    height: undefined,
    alignSelf: 'stretch',
    flex: 1,
  },
});

const tooBig = image =>
  image.width > Dimensions.get('window').width ||
  image.height > Dimensions.get('window').height;

const GiphyThumbnail = ({style, gif}) => {
  const {type, images} = gif;
  const {url} = gif;
  const image =
    type === 'gif'
      ? tooBig(images.fixed_height_still) && images.fixed_height_small_still.url
        ? images.fixed_height_small_still
        : images.fixed_height_still
      : undefined;
  const {url: uri} = image || {};
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Linking.openURL(url);
      }}>
      <View style={style}>
        <If condition={image !== undefined && !!uri}>
          <View style={styles.imageWrap}>
            <Image
              style={styles.thumbnail}
              resizeMode="contain"
              source={{uri}}
            />
            <Text style={styles.imageText}>{url}</Text>
          </View>
        </If>
        <If condition={image !== undefined && !uri}>
          <View style={styles.noDisplay}>
            <Text style={styles.text}>
              cannot display this item{'\n'}
              {'\n'}the URI was missing or malformed
            </Text>
          </View>
        </If>
        <If condition={image === undefined}>
          <View style={styles.noDisplay}>
            <Text style={styles.text}>
              cannot display this item{'\n'}
              {'\n'}it is of type &lsquo;{type}&rsquo;
            </Text>
          </View>
        </If>
      </View>
    </TouchableWithoutFeedback>
  );
};

GiphyThumbnail.propTypes = {
  style: ViewPropTypes.style,
  gif: PropTypes.shape(giphyGIFShape).isRequired,
};

GiphyThumbnail.defaultProps = {
  style: null,
};

export default GiphyThumbnail;

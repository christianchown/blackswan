import * as actions from '../actions';

describe('actions', () => {
  test('SET_ORIENTATION', () => {
    expect(actions.setOrientation('LANDSCAPE')).toMatchSnapshot();
  });
});

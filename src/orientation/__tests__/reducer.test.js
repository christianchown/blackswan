import * as actions from '../actions';
import reducer, {initialState} from '../reducer';

describe('reducer', () => {
  test('SET_ORIENTATION', () => {
    expect(
      reducer(initialState, actions.setOrientation('LANDSCAPE')),
    ).toMatchSnapshot();
  });
  test('SET_ORIENTATION PORTRAITUPSIDEDOWN', () => {
    expect(
      reducer(initialState, actions.setOrientation('PORTRAITUPSIDEDOWN')),
    ).toMatchSnapshot();
  });
});

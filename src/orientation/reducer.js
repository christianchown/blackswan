import {SET_ORIENTATION} from './actions';
import {defaultOrientation} from './propTypes';

export const initialState = defaultOrientation;

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_ORIENTATION: {
      if (action.orientation === 'PORTRAITUPSIDEDOWN') {
        return 'PORTRAIT';
      }
      return action.orientation;
    }
    default: {
      return state;
    }
  }
};

import PropTypes from 'prop-types';

export const Orientation = PropTypes.oneOf([
  'unknown',
  'LANDSCAPE',
  'PORTRAIT',
]);
export const defaultOrientation = 'unknown';

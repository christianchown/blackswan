export const SET_ORIENTATION = 'SET_ORIENTATION';

export const setOrientation = orientation => ({
  type: SET_ORIENTATION,
  orientation,
});

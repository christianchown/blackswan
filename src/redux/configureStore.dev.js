import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'remote-redux-devtools'; // eslint-disable-line import/no-extraneous-dependencies
import reduxThunk from 'redux-thunk';

const configureStore = (reducer, initialState) => {
  const compose = composeWithDevTools({realtime: true});
  const enhancer = compose(applyMiddleware(reduxThunk));
  return createStore(reducer, initialState, enhancer);
};

export default configureStore;

import {combineReducers} from 'redux';

import configureStore from './configureStore';

let store;

const ReduxReactNative = {
  init: (reducers = {}) => {
    const rootReducer = combineReducers(reducers);
    store = configureStore(rootReducer, {});
  },
  store: () => store,
};

export default ReduxReactNative;

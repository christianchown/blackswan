import {compose, createStore, applyMiddleware} from 'redux';
import reduxThunk from 'redux-thunk';

const configureStore = (reducer, initialState) => {
  const enhancer = compose(applyMiddleware(reduxThunk));
  return createStore(reducer, initialState, enhancer);
};

export default configureStore;

import React from 'react';
import {StatusBar} from 'react-native';
import PropTypes from 'prop-types';
import {Provider} from 'react-redux';
import Orientation from 'react-native-orientation';

import {setOrientation} from './orientation/actions';
import GiphyViewer from './components/GiphyViewer';
import config from './config';

class Framework extends React.Component {
  componentDidMount() {
    const {store} = this.props;
    const {dispatch} = store;
    const initial = Orientation.getInitialOrientation();
    dispatch(setOrientation(initial));
    Orientation.unlockAllOrientations();
    Orientation.addOrientationListener(this.orientationDidChange);
  }

  orientationDidChange = newOrientation => {
    const {store} = this.props;
    const {dispatch} = store;
    dispatch(setOrientation(newOrientation));
  };

  render() {
    const {store} = this.props;
    return (
      <Provider store={store}>
        <React.Fragment>
          <StatusBar
            backgroundColor={config.colours.darkBG}
            barStyle="light-content"
          />
          <GiphyViewer />
        </React.Fragment>
      </Provider>
    );
  }
}

Framework.propTypes = {
  store: PropTypes.shape({dispatch: PropTypes.func.isRequired}).isRequired,
};

export default Framework;

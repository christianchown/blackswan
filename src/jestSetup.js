import {Platform} from 'react-native';
/* eslint-disable import/no-extraneous-dependencies */
import {configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import fetchMock from 'jest-fetch-mock';
/* eslint-enable import/no-extraneous-dependencies */

global.fetch = fetchMock;
jest.mock('Platform');
jest.mock('Linking');
jest.mock('NativeAnimatedHelper');
jest.useFakeTimers();
Platform.setOS('ios');

configure({adapter: new Adapter()});

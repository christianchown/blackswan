import {Platform} from 'react-native';

const fontNames = {
  android: {
    lato: 'Lato-Regular',
    latoBold: 'Lato-Bold',
    latoItalic: 'Lato-Italic',
    latoLight: 'Lato-Light',
  },
  ios: {
    lato: 'Lato',
    latoBold: 'Lato Bold',
    latoItalic: 'Lato Italic',
    latoLight: 'Lato Light',
  },
};

const {lato, latoBold, latoItalic, latoLight} = fontNames[Platform.OS];

const config = {
  /* eslint-disable global-require */
  images: {
    blackswan: require('../assets/blackswan.png'),
    logo: require('../assets/logo.png'),
    loading: require('../assets/loading.png'),
  },
  /* eslint-enable global-require */
  colours: {
    lightText: '#A8A6A6',
    midText: '#5D5D5D',
    darkText: '#000000',
    darkBG: '#2D2D2D',
    lightBG: '#F1F1F1',
  },
  fonts: {
    lato,
    latoBold,
    latoItalic,
    latoLight,
  },
  giphyAPIKey: 'Ee1Ap3RhI2kBXyzmdd6DbjsC9cQVKJY1', // dc6zaTOxFJmzC
};

export default config;

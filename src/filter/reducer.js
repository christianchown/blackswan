import {UPDATE_TEXT, FOCUS_TEXT} from './actions';

export const initialState = {
  text: '',
  focused: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_TEXT: {
      return {
        ...state,
        text: action.text,
      };
    }
    case FOCUS_TEXT: {
      return {
        ...state,
        focused: action.focus,
      };
    }
    default: {
      return state;
    }
  }
};

export const UPDATE_TEXT = 'UPDATE_TEXT';
export const FOCUS_TEXT = 'FOCUS_TEXT';

export const updateText = text => ({
  type: UPDATE_TEXT,
  text,
});

export const focusText = focus => ({
  type: FOCUS_TEXT,
  focus,
});

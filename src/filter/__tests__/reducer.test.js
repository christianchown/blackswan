import * as actions from '../actions';
import reducer, {initialState} from '../reducer';

describe('reducer', () => {
  test('UPDATE_TEXT', () => {
    expect(reducer(initialState, actions.updateText('text'))).toMatchSnapshot();
    expect(
      reducer(initialState, actions.updateText('other')),
    ).toMatchSnapshot();
  });
  test('FOCUS_TEXT', () => {
    expect(reducer(initialState, actions.focusText(true))).toMatchSnapshot();
    expect(reducer(initialState, actions.focusText(false))).toMatchSnapshot();
  });
});

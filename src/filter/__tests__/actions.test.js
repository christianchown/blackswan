import * as actions from '../actions';

describe('actions', () => {
  test('UPDATE_TEXT', () => {
    expect(actions.updateText('text')).toMatchSnapshot();
    expect(actions.updateText('other')).toMatchSnapshot();
  });
  test('FOCUS_TEXT', () => {
    expect(actions.focusText(true)).toMatchSnapshot();
    expect(actions.focusText(false)).toMatchSnapshot();
  });
});

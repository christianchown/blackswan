import React from 'react';
import SplashScreen from 'react-native-splash-screen';

import ReduxReactNative from './redux/ReduxReactNative';
import filter from './filter/reducer';
import giphy from './giphy/reducer';
import orientation from './orientation/reducer';
import Framework from './Framework';

ReduxReactNative.init({filter, giphy, orientation});
const store = ReduxReactNative.store();

class App extends React.Component {
  componentDidMount() {
    SplashScreen.hide();
  }

  render() {
    return <Framework store={store} />;
  }
}

export default App;
